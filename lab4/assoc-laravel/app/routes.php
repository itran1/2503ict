<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Load sample data, an array of associative arrays. */
require "models/pms.php";

// Display search form
Route::get('/', function()
{
	return View::make('pms.query');
});

// Perform search and display results
Route::get('search', function()
{
  $searchPm = Input::get('searchPm1');
 // $year = Input::get('year');
  //$state = Input::get('state');
  
  $results = search($searchPm);
  
	return View::make('pms.results')->withPms($results)->with('searchPm', $searchPm);
});



/* Functions for PM database example. */

/* Search sample data for $name or $year or $state from form. */
function search($searchPm) {
  $pms = getPms();

  // Filter $pms by $name
  if (!empty($searchPm)) {
    $results = array();
    foreach ($pms as $pm) {
      if (stripos($pm['name'], $searchPm) !== FALSE or 
	    	  stripos($pm['address'], $searchPm) !== FALSE or 
	    	  stripos($pm['phone'], $searchPm) !== FALSE or 
	    	  stripos($pm['email'], $searchPm) !== FALSE) {
	    	    
        $results[] = $pm;
        
      }
    }
    $pms = $results;
  }
/*
  // Filter $pms by $year
  if (!empty($year)) {
    $results = array();
    foreach ($pms as $pm) {
      if (strpos($pm['from'], $year) !== FALSE || 
          strpos($pm['to'], $year) !== FALSE) {
        $results[] = $pm;
      }
    }
    $pms = $results;
  }

  // Filter $pms by $state
  if (!empty($state)) {
    $results = array();
    foreach ($pms as $pm) {
      if (stripos($pm['state'], $state) !== FALSE) {
        $results[] = $pm;
      }
    }
    $pms = $results;
  }
*/
  return $pms;
}