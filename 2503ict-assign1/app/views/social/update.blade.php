@extends('layouts.master')

@section('main_page')
<li><a id = "home" href="{{{url("home_page")}}}" >Posts</a></li>
<li><a id = "home" href="{{{url("documentation")}}}" >Documentation</a></li>
@stop

@section('post')
<h1>Update Message</h1>
<form method="post" action="{{{ url('update_msg_action') }}}">
            <div id="tit">Title: <br>
                <input id="inputField" type="text" name="title" value="{{{ $msg->title }}}"/><br>
            </div>
            <input type="hidden" name="id" value="{{{ $msg->id }}}"> 
            <div id="nameInput">
                Name: <br>
                <input id="inputField" type="text" name="name" value="{{{ $msg->name}}}" /> <br>
            </div>
            <div id="messageInput">
                Message: <br>
                <textarea id="textarea" rows="4" cols="25" name="message">{{{ $msg->message }}}</textarea> <br>
                
            </div>
            <button>Update</button> 
</form>
@stop

@section('content')
<form method="post" action = "go_back">
          <a type="button" href="{{{ url("go_back") }}}">Cancel</a>
</form>
@stop