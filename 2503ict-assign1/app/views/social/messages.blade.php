@extends('layouts.master')

@section('main_page')
<li><a id = "home" href="{{{home_page}}}" >Posts</a></li>
<li><a id = "home" href="{{{url("documentation")}}}" >Documentation</a></li>
@stop

@section('post')
<form method="post" action="add_post_action">
            <div id="tit">Title: <br>
                <input id="inputField" type="text" name="title"/> <br>
            </div>
            <div id="nameInput">
                Name: <br>
                <input id="inputField" type="text" name="name"/> <br>
            </div>
            <div id="messageInput">
                Message: <br>
                <textarea id="textarea" rows="4" cols="25" name="message" placeholder="Enter your message here"></textarea> <br>
                
            </div>
            <button>Post</button>
</form>
@stop


@section('post')


@foreach ($msgs as $msg)

<div class='post'>
                <input type="hidden" name="id" value="{{{ $msg->id }}}"> 
              <img href='{{{ url("social.input") }}}' class='photo' src='{{{ $msg->img_path }}}' alt='Batman'>
              Title: {{{ $msg->title }}}<br>
              Name: {{{ $msg->name }}} <br>
               Date: {{{ $msg->post_date  }}}  <br>
              <p>Message:  {{{ $msg->message }}} </p> <br>
                <a id="delete" href="{{{ url("delete_msg_action/$msg->id") }}}"> Delete</a> <a id="update" href="{{{ url("update_msg/$msg->id") }}}"> Update</a> 
</div>

@endforeach

@stop