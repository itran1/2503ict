@extends('layouts.master')

@section('main_page')
<li><a id = "home" href="{{{url("home_page")}}}" >Posts</a></li>
<li><a id = "home" href="{{{url("documentation")}}}" >Documentation</a></li>
@stop

@section('post')
<h1>Diagrams</h1>
<a href="images/db.png"><img src="images/db.png" alt="ER diagram of database" height="200" width="200"></img></a> <br>
<a href="images/site.png"><img src="images/site.png" alt="site diagram" height="200" width="200"></img></a>
@stop

@section('content')
<h1>Documentation</h1>
<p id="doc">For the assignment I was able to complete some tasks. I made sure there was a navigation bar,
I made sure that the home page had a form to add a post and shows the posts. Each post has an icon,
title, user's name, a message as well as the date and time when that post was created. Each post has
an update(instead of edit), delete and a View comment link under the main contents of a post. When 
the update link is pressed it goes to another page where they can update it or cancel(redirects to the 
home page). The view comments link goes to another page where comments can be seen. I haven't finished
the comments part of the assignment. You can't post a comment or delete a comment. The only comment that
appears is the default comment hardcoded in the database. All of the site was done with laravel and the
DB class. As for deleting a post when it is deleted you won't be able to post one as it will error. To 
post you will have to either press the post buttton in the navigation bar or update a current post.</p>
@stop