create table if not exists msg (

    id integer not null primary key autoincrement,
    title varchar(80),
    name varchar(80) not null,
    message text default '',
    img_path VARCHAR(80) default '' NOT NULL,
    post_date varchar(80) not null
); 

create table if not exists com (

    c_id integer not null primary key autoincrement,
    name varchar(80) not null,
    comment text default '',
    img_path VARCHAR(80) default '' NOT NULL,
    com_date varchar(80) not null,
    msg_id INTEGER NOT NULL REFERENCES msg(id)
); 

insert into msg values (null, "Who am I?", "Batman",  "I'm Batman", "images/batman.jpg", "21/12/12 4:20 pm");

insert into com values (null, "Batman",  "I'm Batman", "images/batman.jpg", "21/12/12 4:20 pm", 1);
