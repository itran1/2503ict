<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('user/userpage', array('as' => 'user.userpage', 'uses' => 'UserController@userpage'));
Route::get('user/loginpage', array('as' => 'user.loginpage', 'uses' => 'UserController@loginpage'));
// Route::post('user/loginpage', array('as' => 'user.loginpage', 'uses' => 'UserController@loginpage'));


Route::get('message/show_comment', array('as' => 'message.show_comment', 'uses' => 'MessageController@show_comment'));

Route::get('user/documentation', array('as' => 'user.documentation', 'uses' => 'UserController@documentation'));
Route::get('user/home_page', array('as' => 'user.home_page', 'uses' => 'UserController@home_page'));
Route::post('user/login', array('as' => 'user.login', 'uses' => 'UserController@login' ));
Route::get('user/logout', array('as' => 'user.logout', 'uses' => 'UserController@logout' ));
Route::resource('message', 'MessageController');
Route::resource('comment', 'CommentController'); 
Route::resource('user', 'UserController'); 


Route::get('/', function()
{   
    $msgs = Message::all();
	  return View::make('social.input')->withMsgs($msgs);;

});

/*
// gets the IDs
Route::get('social.input/{id}', function ($id)
{
  $msg = get_msg($id);
	return View::make('social.input')->withMsg($msg);
}); 

// selects all of the colomns from the table called msg in the database (posts)
function get_msgs()
{
  $sql = "select * from msg";
  $msgs = DB::select($sql);
  return $msgs;
};

// selects the info from a row from the msg table in the database where the specified ID matches
function get_msg($id)
{
	$sql = "select id, title, name, message, post_date, img_path from msg where id = ?";
	$msgs = DB::select($sql, array($id));
    
	// If we get more than one item or no items display an error
	if (count($msgs) != 1) 
	{
    die("Invalid query or result: $query\n");
  }

	// Extract the first item (which should be the only item)
  $msg = $msgs[0];
	return $msg;
};

// gets info from the textfields and parses them to a function
Route::post('add_post_action', function()
{
  $title = Input::get('title');
  $name = Input::get('name');
  $message = Input::get('message');
  $date = date("d/m/y g:i a");
  $imgPath = "images/batman.jpg";

  $msga = add_msg($title, $name, $message, $date, $imgPath);

  // If successfully created then display newly created item
  if ($msga) 
  {
     $msgs = get_msgs();
    return View::make('social.input')->withMsgs($msgs);
  } 
  else
    die("Error adding item");
  });

// adds another post into the msg table in the database
function add_msg($title, $name, $message, $date, $imgPath) 
{
  $sql = "insert into msg (title, name, message, post_date, img_path) values (?, ?, ?, ?, ?)";

  DB::insert($sql, array($title, $name, $message, $date, $imgPath));

  $id = DB::getPdo()->lastInsertId();

  return $id;
};


// deletes a post with the specified ID
Route::get('delete_msg_action/{id}', function($id)
{
  $del = delete_msg($id);

  if ($del) 
  {
    $msgs = get_msgs();
    return View::make('social.input')->withMsgs($msgs);
   
  } 
  
});

// deletes the specified post from the database
function delete_msg($id)
{
 $sql = "delete from msg where id = ?";
 DB::delete($sql, array($id));

 return $sql;
};

// updates the home page
Route::get('update_msg/{id}', function($id)
{
  $msg = get_msg($id);
  return View::make('social.update')->withMsg($msg);
});

// Gets the information to update/edits the post
Route::post('update_msg_action', function()
{
  $title = Input::get('title');
  $name = Input::get('name');
  $message = Input::get('message');
  $date = date("d/m/y g:i a");
  $id = Input::get('id');
  $up = update_msg($title, $name, $message, $date, $id);

  // If successfully updated then display updated item
  if ($up) 
  {
      
      $msgs = get_msgs();
    return View::make('social.input')->withMsgs($msgs);
    
  } 
  
});

// updates the information of a post in the database
function update_msg($title, $name, $message, $date, $id)
{
 $sql = "update msg set title = ?, name = ?, message = ?, post_date = ?  where id = ?";
 DB::update($sql, array($title, $name, $message, $date, $id));
 
 return $sql;
};

// specifically made for the cancel button in the update page that goes back to the home page without updating the post
Route::get('go_back', function()
{
    $msgs = get_msgs();
    return View::make('social.input')->withMsgs($msgs);
});

// creates the comments page
Route::get('view_comments', function()
  {
    //$msgs = get_msg($id);
    $coms = get_coms();
    return View::make('social.comments')->withComs($coms);
  });
  
// gets the comments stored within the database
function get_coms()
{
  $sql = "select * from com";
  $coms = DB::select($sql);
  return $coms;
};
*/

// made for the Post button in the navigation bar to go to the home page
Route::get('home_page', function()
{   
    $msgs = DB::table('messages')->get();
		return View::make('social.input')->withMsgs($msgs);

});

// makes the documentation page
Route::get('documentation', function()
  {
    return View::make('social.documentation');
  });
