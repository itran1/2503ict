<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
	
		// $msgs = Message::all();
		$msgs = Message::where('user_id', '=', Auth::user()['id'])->get();
		// $msgs = Message::where('user_id = ?', array($uid));
		// $msgs = Message::whereRaw('messages.user_id = ?', array($id))->get();
		return View::make('social.input')->withMsgs($msgs);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		$input = Input::all();
	
		$vc = Validator::make($input, User::$createrules);
		
		$user = User::where('email', '=', Input::get('email'))->first();
		
		if($user == null) {
			
		 	if ($vc->passes()) {
		 		$created = 'Account created successfully';
				$password = $input['password'];
				$encrypted = Hash::make($password);
				$user = new User;
				$user->email = $input['email'];
				$user->password = $encrypted;
				$user->fullname = $input['fullname'];
				$user->date_of_birth = $input['dob'];
				$user->profile_image = 'images/batman.jpg';
				$user->save();
				return Redirect::action('user.loginpage')->with('ok', $created);
			
			} else {
			
				return Redirect::action('UserController@loginpage')->withErrors($vc);
			
			}
			
		} else {
			return Redirect::action('UserController@loginpage')->withErrors($vc);
			
		}
			
	
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if (!Auth::check()) return Redirect::route('user.index');
		
		$user = User::find($id);
		return View::make('social.user')->with('user', $user); 
		
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if (!Auth::check()) return Redirect::route('user.index');
		
		$input = Input::all();
		
		$v = Validator::make($input, User::$updaterules);
		
		if ($v->passes()){
			
			$user = User::find($id);
			$user->email = $input['email'];
			$user->password = $encrypted;
			$user->fullname = $input['fullname'];
			$user->date_of_birth = $input['dob'];
			$user->profile_image = 'images/batman.jpg';
			$user->save();
			return Redirect::action('UserController@edit');
			
		} else{
			// Show validation errors
		
			return Redirect::action('UserController@edit', $id)->withErrors($v);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function documentation()
	{
		return View::make('social.documentation');
	}
	
	public function home_page()
	{
		$input = Input::get('id');
		return Redirect::action('UserController@index')->withUid($input);
	}
	
	public function login()
	{	
		$input = Input::all();
		
		$vl = Validator::make($input, User::$loginrules);
		
		$loginData = array(
			'email' => Input::get('emailLogin'),
			
			'password' => Input::get('passwordLogin')
		);
		
		if (Auth::attempt($loginData))
		{
			return Redirect::action('UserController@index');
			// return Redirect::to(URL::previous());
		} else {
			return Redirect::to(URL::previous())->withInput()->withErrors($vl);
		}
	}
	
	public function logout()
	{
		Auth::logout();
		return Redirect::action('UserController@index');
	}
	
	public function loginpage()
	{
		return View::make('social.login');
	}
	
	
	public function userpage($id)
	{
		$userD = User::find($id);
		return View::make('social.userpage')->with('userD', $userD);
	}
	
}
