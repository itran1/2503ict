<?php

class CommentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
		//$msgid = Input::get('id');
		
		//$msg = Message::where('id', '=', 'comments.id');
		 $coms = Comment::all();
		// $coms = Comment::where('id' , '=' , $msg['id'])->get();
		// Comment::where('message_id', '=', '?', array($msgid))->first();

		return View::make('social.comments')->with('coms', $coms);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if (!Auth::check()) return Redirect::route('user.index');
		
		$input = Input::all();
		
		$vc = Validator::make($input, Comment::$comrules);
			
		 	if ($vc->passes()) {
		
				$com = new Comment;
				$com->c_name = 'Batman';
				$com->comment = $input['comment'];
				$com->img_path = 'images/batman.jpg';
				$com->message_id = 1;
				$com->save();
				
				return Redirect::action('CommentController@index');
			
			} else {
			
				return Redirect::action('CommentController@index')->withErrors($vc);
			
			}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{	
		// $msgs = DB::table('messages')
  //          ->join('comments', 'id', '=', 'comments.message_id')
  //          ->select('message.id')
  //          ->get();
		
		// $coms = Comment::all();
		// return View::make('social.comments')->with('msgs', $msgs)->with('coms', $coms);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$com = Comment::find($id);
		$com->delete();
		
		return Redirect::action('CommentController@index');
	}


}
