<?php

class MessageController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$msgs = Message::all();
		return View::make('social.input')->withMsgs($msgs);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if (!Auth::check()) return Redirect::route('user.index');
		
		
		$input = Input::all();
	
		$vc = Validator::make($input, Message::$createrules);
			
		 	if ($vc->passes()) {
		 		
				$msg = new Message;
				$msg->m_name = $input['title'];
				$msg->message = $input['message'];
				$msg->img_path = 'images/batman.jpg';
				$msg->user_id = $input['id'];
				$msg->save();
				
				return Redirect::action('UserController@home_page');
			
			} else {
			
				return Redirect::action('UserController@index')->withErrors($vc);
			
			}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if (!Auth::check()) return Redirect::route('user.index');
		
		$messages = Message::find($id);
		return View::make('social.update')->with('messages', $messages); 
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	 
	public function update($id)
	{
		if (!Auth::check()) return Redirect::route('user.index');
		
		$input = Input::all();
		
		$v = Validator::make($input, Message::$updaterules);
		
		if ($v->passes()){
			
			$messages = Message::find($id);
			$messages->message = $input['message'];
			$messages->save();
		
			return Redirect::action('UserController@index');
			
		} else{
			// Show validation errors
		
			return Redirect::action('MessageController@edit', $id)->withErrors($v);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if (!Auth::check()) return Redirect::route('user.index');
	 	$msg = Message::find($id);
		$msg->delete();
		$msgs = DB::table('messages')->get();
		return Redirect::action('UserController@index');
	}
	
	public function show_comment($id) 
	{
		$message = Message::find($id);
		 return Redirect::action('CommentController@index')->with('msgs', $message);
	}
	

}
