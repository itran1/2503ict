@extends('layouts.master')

@section('main_page')
<li>{{link_to_route('user.home_page', 'Posts')}}</li>
<li><a id = "home" href="{{{url("documentation")}}}" >Documentation</a></li>
<li><a id = "home" href="{{{url("friends")}}}" >Friends</a></li>
@if (Auth::check())
        
   <li>{{ link_to_route('user.edit', Auth::user()->email) }}</li> <li> {{ link_to_route('user.logout', "(Sign out)" ) }}</li>
            
@else

    <li>{{ link_to_route('user.loginpage', 'Login') }}</li>
    
@endif
@stop

@section('post')

            
            {{ Form::open(array('action' => 'CommentController@store', 'class' => 'post')) }}
                {{ Form::label('comment', 'Comment ') }}
                {{ Form::textarea('comment', null, array('class' => 'textarea', 'size' => '25x4')) }}
                {{ $errors->first('comment') }}
                <p></p>
                
                {{ Form::submit('Add Comment', ['id' => 'submit']) }}
            {{ Form::close() }}


@stop

@section('content')
<h1>Comments</h1>

@foreach($coms as $com)

<div class='post'>
                {{ Form::hidden('id', $com->id) }}
              <img href='{{{ url("social.input") }}}' class='photo' src='{{{ $com->img_path }}}' alt='Batman'>
               Date: {{{ $com->created_at  }}}  <br>
              <p>Comment:  {{{ $com->comment }}} </p> <br>
               {{ Form::open(array('method' => 'DELETE', 'route' => array('comment.destroy', $com->id))) }}
              {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
              {{ Form::close() }}
</div>

@endforeach

@stop

