@extends('layouts.master')

@section('main_page')
<li>{{link_to_route('user.home_page', 'Posts')}}</li>
<li><a id = "home" href="{{{url("documentation")}}}" >Documentation</a></li>
<li><a id = "home" href="{{{url("friends")}}}" >Friends</a></li>

@if (Auth::check())
        
   <li>{{ link_to_route('user.edit', Auth::user()->email) }}</li> <li> {{ link_to_route('user.logout', "(Sign out)" ) }}</li>
            
@else

    <li>{{ link_to_route('user.loginpage', 'Login') }}</li>
    
@endif

@stop


@section('post')

@if (Auth::check())
        
    {{ Form::open(array('action' => 'MessageController@store', 'class' => 'post')) }}
                {{ Form::hidden('id', Auth::user()->id) }}
                {{ Form::label('title', 'Title: ') }}
                </br>
                {{ Form::text('title', null, array('class' => 'textarea')) }}
                {{ $errors->first('title') }}
                <p></p>
                
                {{ Form::label('message', 'Message: ') }}
                {{ Form::textarea('message', '', array('class' => 'textarea', 'size' => '25x4') ) }}
                {{ $errors->first('message') }}
                
                <p></p>
                {{ Form::submit('Add Message', ['id' => 'submit']) }}
            {{ Form::close() }}
    
@endif
        
           


@stop



@section('content')

@foreach ($msgs as $msg)

<div class='post'>
                {{ Form::hidden('id', $msg->id)}}
                
              <img href='{{{ url("social.input") }}}' class='photo' src='{{{ $msg->img_path }}}' alt='Batman'>
              Title: {{{ $msg->m_name }}}<br>
               Date: {{{ $msg->created_at }}}  <br>
              <p>Message:  {{{ $msg->message }}} </p>
              {{ Form::open(array('method' => 'DELETE', 'route' => array('message.destroy', $msg->id))) }}
              {{ Form::submit('Delete', array('id' => 'submit')) }}
              {{ Form::close() }}
              
              {{ Form::open(array('action' => 'CommentController@index')) }}
                  {{ Form::submit('Comments', ['id' => 'submit']) }}
              {{ Form::close() }}
              
              <button id="submit">{{ link_to_route('message.edit', 'Update', $msg->id) }}</button>
              
              
              
             
             
</div>

@endforeach

@stop

