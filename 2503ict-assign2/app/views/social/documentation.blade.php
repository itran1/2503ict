@extends('layouts.master')

@section('main_page')
<li>{{link_to_route('user.home_page', 'Posts')}}</li>
<li><a id = "home" href="{{{url("documentation")}}}" >Documentation</a></li>
<li><a id = "home" href="{{{url("friends")}}}" >Friends</a></li>
@if (Auth::check())
        
   <li>{{ link_to_route('user.edit', Auth::user()->email) }}</li> <li> {{ link_to_route('user.logout', "(Sign out)" ) }}</li>
            
@else

    <li>{{ link_to_route('user.loginpage', 'Login') }}</li>
    
@endif
@stop

@section('post')
<h1>Diagrams</h1>
<a href="images/db.png"><img src="images/db.png" alt="ER diagram of database" height="200" width="200"></img></a> <br>
<a href="images/site.png"><img src="images/site.png" alt="site diagram" height="200" width="200"></img></a>
@stop

@section('content')
<h1>Documentation</h1>
<p id="doc">For the 2nd assignment I was able to complete some tasks. I was ablt to complete task 1
even though my 1st assignment was finished i still managed to get all of what was needed in the 1st assignment 
and had it in this assignment. As for task 2 i was only able to complete some of it like adding user functionality
where you can create an account and login. Also to be able to create and posts or comments you have to be logged in.
I have also implemented validation for the textfield thoughout the site.
I have also pre seeded the databases. As for friends i have not implemented anything for it or any image handling 
as stapler broke my assignment when i installed it so i decided to to risk it again. There is no pagination either.</p>
@stop