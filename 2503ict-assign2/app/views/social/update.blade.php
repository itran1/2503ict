@extends('layouts.master')

@section('main_page')
<li>{{link_to_route('user.home_page', 'Posts')}}</li>
<li><a id = "home" href="{{{url("documentation")}}}" >Documentation</a></li>
<li><a id = "home" href="{{{url("friends")}}}" >Friends</a></li>
@if (Auth::check())
        
   <li>{{ link_to_route('user.edit', Auth::user()->email) }}</li> <li> {{ link_to_route('user.logout', "(Sign out)" ) }}</li>
            
@else

    <li>{{ link_to_route('user.loginpage', 'Login') }}</li>
    
@endif
@stop

@section('post')
<h1>Update Message</h1>

{{Form::model( $messages, ['method' => 'put', 'route' => array( 'message.update', $messages['id'])] ) }}

{{ Form::label('message', 'Message: ') }}
{{ Form::textarea('message', $messages['message'], array('class' => 'textarea') ) }}
{{ $errors->first('message') }}
<p></p>

{{ Form::submit('Update') }}

{{ Form::close() }}

@stop

@section('content')

{{ link_to_route('user.home_page', 'Cancel') }}

@stop