@extends('layouts.master')

@section('main_page')

<li>{{link_to_route('user.home_page', 'Posts')}}</li>
<li><a id = "home" href="{{{url("documentation")}}}" >Documentation</a></li>
<li><a id = "home" href="{{{url("friends")}}}" >Friends</a></li>

@if (Auth::check())
        
   <li>{{ link_to_route('user.edit', Auth::user()->email) }}</li> <li> {{ link_to_route('user.logout', "(Sign out)" ) }}</li>
            
@else

    <li>{{ link_to_route('user.loginpage', 'Login') }}</li>
    
@endif

@stop

@section('post')
<h1>Welcome {{Auth::user()->fullname}}</h1>
<img src="{{ Auth::user()->profile_image }}"></img>
<p>Date of Birth: {{Auth::user()->date_of_birth}}</p>
@stop

@section('content')
<h1>Edit Details</h1>
{{Form::model(['method' => 'PUT', 'route' => array( 'user.update', Auth::user()->id )] ) }}
    {{ Form::label('fullname', 'Full Name: ') }}
    {{ Form::text('fullname', Auth::user()->fullname, ['class' => 'textarea']) }}
    {{ $errors->first('fullname') }}
    <p></p>
    {{ Form::label('dob', 'Date of birth: ') }}
    {{ Form::input('date', 'dob', Auth::user()->date_of_birth, ['class' => 'textarea']) }}
    {{ $errors->first('dob') }}
    <p></p>
    {{ Form::label('passwordcreate', 'Password: ') }}
    {{ Form::password('password', null, ['class' => 'textarea']) }}
    {{ $errors->first('password') }}
    <p></p>
    {{ Form::label('dp', 'Profile Picture: ') }}
    {{ Form::file('dp') }}
    {{ $errors->first('dob') }}
    <p></p>
    {{ Form::submit('Update', ['id' => 'submit']) }}
{{ Form::close() }}

@stop