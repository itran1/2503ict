@extends('layouts.master')

@section('main_page')
<li>{{link_to_route('user.home_page', 'Posts')}}</li>
<li><a id = "home" href="{{{url("documentation")}}}" >Documentation</a></li>
<li><a id = "home" href="{{{url("friends")}}}" >Friends</a></li>
@if (Auth::check())
        
   <li>{{ link_to_route('user.edit', Auth::user()->email, Auth::user()->id) }}</li> <li> {{ link_to_route('user.logout', "(Sign out)" ) }}</li>
            
@else

    <li>{{ link_to_route('user.loginpage', 'Login') }}</li>
    
@endif
@stop

@section('post')
<h1>Login</h1>
            
        
            
            
        
            {{ Form::open(array('url' => secure_url('user/login'), 'class' => 'post')) }}
                {{ Form::label('emailLogin', 'Email: ') }}
                {{ Form::email('emailLogin', null, ['class' => 'textarea']) }}
                {{ $errors->first('emailLogin') }}
                <p></p>
                {{ Form::label('passwordLogin', 'Password: ') }}
                {{ Form::password('passwordLogin', ['class' => 'textarea']) }}
                {{ $errors->first('passwordLogin') }}
                <p></p>
                {{ Form::submit('Sign in', ['id' => 'submit']) }}
            {{ Form::close() }}
            
    

@stop

@section('content')

<h1>Create Account</h1>

{{ Form::open(array('url' => secure_url('user'), 'files' => 'true', 'class' => 'post')) }}
    {{ Form::label('email', 'email: ') }}
    {{ Form::email('email', null, ['class' => 'textarea']) }}
    {{ $errors->first('email') }}
    <p></p>
    {{ Form::label('fullname', 'Full Name: ') }}
    {{ Form::text('fullname', null, ['class' => 'textarea']) }}
    {{ $errors->first('fullname') }}
    <p></p>
    {{ Form::label('dob', 'Date of birth: ') }}
    {{ Form::input('date', 'dob', null, ['class' => 'textarea']) }}
    {{ $errors->first('dob') }}
    <p></p>
    {{ Form::label('passwordcreate', 'Password: ') }}
    {{ Form::password('password', null, ['class' => 'textarea']) }}
    {{ $errors->first('password') }}
    <p></p>
    {{ Form::label('dp', 'Profile Picture: ') }}
    {{ Form::file('dp') }}
    {{ $errors->first('dob') }}
    <p></p>
    {{ Form::submit('Create', ['id' => 'submit']) }}
{{ Form::close() }}

@stop