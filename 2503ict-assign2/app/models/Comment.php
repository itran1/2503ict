<?php

  class Comment extends Eloquent{
    
    function messages() {
        
        return $this->belongsTo('Message');
        
    }
    
    function ucomments() {
        
        return $this->belongsTo('User');
        
    }
    
    public static $comrules = array(
            
            'comment' => 'required|min:3'
    );
    
  }

?>