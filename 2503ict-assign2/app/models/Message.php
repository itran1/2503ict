<?php

  class Message extends Eloquent{
    
    function users() {
        
        return $this->belongsTo('User');
        
    }
    
    function hascomments() {
        
        return $this->hasMany('Comment');
        
    } 
    
     public static $updaterules = array(
            
            'message' => 'required|min:3'
    );
    
    public static $createrules = array(
            'title' => 'required',
            'message' => 'required|min:3'
    );
    
    
  }

