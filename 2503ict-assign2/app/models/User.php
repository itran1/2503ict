<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	
	function hasmessage() {
        
        return $this->hasMany('Message');
        
    }
    
    function usercomment() {
        
        return $this->hasMany('Comment');
        
    }
    
    public static $createrules = array(
            'email' => 'required|unique:users',
            'fullname' => 'required',
            'password' => 'required|min:6',
            'dob' => 'required'
    );
    
    public static $loginrules = array(
            'emailLogin' => 'required|min:3',
            'passwordLogin' => 'required'

    );
    
    public static $updaterules = array(
            'fullname' => 'required',
            'password' => 'required|min:6',
            'dob' => 'required'
    );
}
