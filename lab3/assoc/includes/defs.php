<?php
/* Functions for PM database example. */

/* Load sample data, an array of associative arrays. */
include "pms.php";

/* Search sample data for $name or $year or $state from form. */
function search($name, $year, $state) {
    global $pms; 

    // Filter $pms by $name
    if (!empty($name) or !empty($year) or !empty($state)) {
	$results = array();
	foreach ($pms as $pm) {
		
	    if (stripos($pm['name'], $name) !== FALSE or 
	    	stripos($pm['from'], $year) !== FALSE or 
	    	stripos($pm['to'], $year) !== FALSE or 
	    	stripos($pm['state'], $state) !== FALSE) {
	    		
		$results[] = $pm;
		
	    }
	}
	$pms = $results;
    }

    return $pms;
}

?>
