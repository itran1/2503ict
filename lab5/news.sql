CREATE TABLE IF NOT EXISTS Issues (
	I_Id INTEGER PRIMARY KEY,
	Issue_Num INTEGER
);

CREATE TABLE IF NOT EXISTS Articles (
	A_Id INTEGER PRIMARY KEY,
	Headline VARCHAR(40) default '' NOT NULL,
	Text VARCHAR(255) default '' NOT NULL,
	Author VARCHAR(40) default '' NOT NULL,
	issues_id INTEGER NOT NULL REFERENCES Issues(I_Id)
);

CREATE TABLE IF NOT EXISTS Photos (
	P_ID INTEGER PRIMARY KEY,
	Caption VARCHAR(255) default '' NOT NULL,
	Photographer VARCHAR(40) default '' NOT NULL,
	Image_Path VARCHAR(45) default '' NOT NULL
);

INSERT INTO Issues(Issue_Num)
	VALUES 
	(69);
	
INSERT INTO Issues(Issue_Num)
	VALUES 
	(70);
	
INSERT INTO Articles(Headline, Text, Author, issues_id)
	VALUES 
	("Dead Man Dies", "A man has been found dead", "Alyve Man", 1);
	
INSERT INTO Articles(Headline, Text, Author, issues_id)
	VALUES 
	("Surivor of plane wreckage found", "A man has been rescued from a plane crash", "Survye Verr", 2);
	
INSERT INTO Articles(Headline, Text, Author, issues_id)
	VALUES 
	("Cure for cancer?", "A possible cure for cancer has been found at the Griffith University in Queensland Australia", "Dr. Heel", 1);

INSERT INTO Articles(Headline, Text, Author, issues_id)
	VALUES 
	("New poisonous plant found", "A new poisonous plant has been discovored and is said to be able to wipe out an entire city", "Dr. Deth", 2);
	
INSERT INTO Photos(Caption, Photographer, Image_Path)
	VALUES 
	("dead body", "Mr. Arive", "images/deadbody.jpg");

INSERT INTO Photos(Caption, Photographer, Image_Path)
	VALUES 
	("Plane Wreckage", "Raplh Wrekit", "images/rekt.jpg");
	
INSERT INTO Photos(Caption, Photographer, Image_Path)
	VALUES 
	("The students who found the cure and their professor", "David Chen", "images/students.jpg");
	
INSERT INTO Photos(Caption, Photographer, Image_Path)
	VALUES 
	("Poisonous plant", "Eve Tree", "images/plant.jpg");
	