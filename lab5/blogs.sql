CREATE TABLE IF NOT EXISTS Blogs (
	B_Id INTEGER PRIMARY KEY,
	Title VARCHAR(40),
	Creator VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS Articles (
	A_Id INTEGER PRIMARY KEY,
	Headline VARCHAR(45),
	Text VARCHAR(255),
	Author VARCHAR(255),
	blogs_id INTEGER NOT NULL REFERENCES Blogs(B_Id)
);


INSERT INTO Blogs(Title, Creator)
	VALUES
	("The Rich Man", "Dr. Edward Nigma");

INSERT INTO Blogs(Title, Creator)
	VALUES
	("The Dark Knight", "Dr. Edward Nigma");

INSERT INTO Blogs(Title, Creator)
	VALUES
	("The Funny One", "Dr. Edward Nigma");
	
INSERT INTO Blogs(Title, Creator)
	VALUES
	("The Sidekick", "Dr. Edward Nigma");
	
INSERT INTO Blogs(Title, Creator)
	VALUES
	("Holidays in Japan", "Dr. Toyota Toriyama");
	
INSERT INTO Articles(HeadLine, Text, Author, blogs_id)
	VALUES
	("Wayne loses company", "Ex billionare Bruce Wayne has lost his company and has dissappeared", "Dr. Edward Nigma", 1);
	
INSERT INTO Articles(HeadLine, Text, Author, blogs_id)
	VALUES
	("Batman no more?", "The so called Dark Knight has dissappeared after the fall of the Joker", "Dr. Edward Nigma", 2);
	
INSERT INTO Articles(HeadLine, Text, Author, blogs_id)
	VALUES
	("Joker is DEAD", "The Joker is finally dead. Do we have the Batman for this?", "Dr. Edward Nigma", 3);
	
INSERT INTO Articles(HeadLine, Text, Author, blogs_id)
	VALUES
	("Batty's Sidekick gone too?", "The so called Robin is now gone as well. Does this have to fdo with anything about this so called Nightwing?", "Dr. Edward Nigma", 4);
	
INSERT INTO Articles(HeadLine, Text, Author, blogs_id)
	VALUES
	("Having a holiday in Japan?", "Thinking about where you want to go in your next holiday? Why not go visit Japan", "Dr. Toyota Toriyama", 5);