<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		return View::make('users.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
	
		$vc = Validator::make($input, User::$createrules);
		
		$user = User::where('username', '=', Input::get('username'))->first();
		
		if($user == null) {
			
		 	if ($vc->passes()) {
				$password = $input['password'];
				$encrypted = Hash::make($password);
				$user = new User;
				$user-> username = $input['username'];
				$user->password = $encrypted;
				$user->save();
				return Redirect::route('product.index');
			
			} else {
			
				return Redirect::action('UserController@create')->withErrors($vc);
			
			}
			
		} else {
			return Redirect::action('UserController@create')->withErrors($vc);
			
		}
		
		// if ($vl->passes() && ) {
		// 	$password = $input['password'];
		// 	$encrypted = Hash::make($password);
		// 	$user = new User;
		// 	$user-> username = $input['username'];
		// 	$user->password = $encrypted;
		// 	$user->save();
		// 	return Redirect::route('product.index');
		// } else {
		// 	return Redirect::action('UserController@create')->withErrors($vl);
		// }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function login()
	{	
		$input = Input::all();
		
		$vl = Validator::make($input, User::$loginrules);
		
		$loginData = array(
			'username' => Input::get('usernameLogin'),
			
			'password' => Input::get('passwordLogin')
		);
		
		if (Auth::attempt($loginData))
		{
			return Redirect::to(URL::previous());
		} else {
			return Redirect::to(URL::previous())->withInput()->withErrors($vl);
		}
	}
	
	public function logout()
	{
		Auth::logout();
		return Redirect::action('ProductController@index');
	}
}
