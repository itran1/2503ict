<?php

class ProductController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$products = Product::all();
		return View::make('product.index')->with('products', $products); 
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	 
	public function store()
	{	
		if (!Auth::check()) return Redirect::route('product.index');
		
		$input = Input::all();
		
		$product = Product::all();
		$product->name = $input['name'];
		$product->price = $input['price'];
		$product->save();
		return Redirect::route('product.show', $product->id);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	 
	public function show($id)
	{
		
		$product = Product::find($id); 
		
		return View::make('product.show')->with('product', $product);
		
		// foreach ($products as $product)
		// var_dump($product->name);
		// exit();
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$product = Product::find($id); 
		
		return View::make('product.edit')->with('product', $product);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if (!Auth::check()) return Redirect::route('product.index');
		
		$input = Input::all();
		
		$v = Validator::make($input, Product::$rules);
		
		if ($v->passes()){
			
			$product = Product::find($id);
			$product->name = $input['name'];
			$product->price = $input['price'];
			$product->save();
			return Redirect::route('product.show', $product->id);
			
		} else{
			// Show validation errors
		
			return Redirect::action('ProductController@edit', $id)->withErrors($v);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
