@extends('product.layout')



@section('content')
<h1>Product Details</h1>

<p>Name: {{{$product->name}}}</p> 
<p>Price: ${{{$product->price}}}</p>
<p>{{link_to_route('product.edit', 'Edit', array($product->id)) }}</p>


@stop