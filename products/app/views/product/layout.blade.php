<html>
    <head>
        
    </head>
    
    <body>
        @if (Auth::check())
        
            {{ Auth::user()->username }} {{ link_to_route('user.logout', "(Sign out)" ) }}
            
        @else
        
            {{ Form::open(array('url' => secure_url('user/login'))) }}
                {{ Form::label('usernameLogin', 'Username: ') }}
                {{ Form::text('usernameLogin') }}
                {{ $errors->first('usernameLogin') }}
                
                {{ Form::label('passwordLogin', 'Password: ') }}
                {{ Form::text('passwordLogin') }}
                {{ $errors->first('passwordLogin') }}
                
                {{ Form::submit('Sign in') }}
            {{ Form::close() }}
            
            <p>If you don't have an account then {{ link_to_route('user.create', "Create an account" ) }}</p>
        @endif
        
        @section('content')
           
            
        @show
       
    </body>
</html>