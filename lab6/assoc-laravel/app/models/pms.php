<?php
/* Australian Prime Ministers.  Data as of 5 March 2010. */
function getPms()
{
  $pms = array(
      array('index' => '1', 'name' => 'Edmund Barphonen', 'address' => '11 Prime st.', 'phone' => '1234 5678', 'email' => 'e.barphonen@aol.com'),
      array('index' => '2', 'name' => 'Alfred Deakin', 'address' => '12 Prime st.', 'phone' => '2345 6789', 'email' => 'a.deakin@aol.com'),
      array('index' => '3', 'name' => 'Chris Watson', 'address' => '13 Prime st.', 'phone' => '3456 7890', 'email' => 'c.watson@gmail.com'),
      array('index' => '4', 'name' => 'George Reid', 'address' => '14 Prime st.', 'phone' => '4567 8901', 'email' => 'g.reid@yahoo.com'),
      array('index' => '-', 'name' => 'Alfred Deakin', 'address' => '12 Prime st.', 'phone' => '5678 9012', 'email' => 'a.deakin@aol.com'),
      array('index' => '5', 'name' => 'Andrew Fisher', 'address' => '16 Prime st.', 'phone' => '6789 0123', 'email' => 'a.fisher@gmail.com'),
      array('index' => '-', 'name' => 'Alfred Deakin', 'address' => '12 Prime st.', 'phone' => '7890 1234', 'email' => 'a.deakin@hotmail.com'),
      array('index' => '-', 'name' => 'Andrew Fisher', 'address' => '18 Prime st.', 'phone' => '8901 2345', 'email' => 'a.fisher@gmail.com'),
      array('index' => '6', 'name' => 'Joseph Cook', 'address' => '19 Prime st.', 'phone' => '9012 3456', 'email' => 'j.cook@hotmail.com'),
      array('index' => '-', 'name' => 'Andrew Fisher', 'address' => '20 Prime st.', 'phone' => '0123 4567', 'email' => 'a.fisher@gmail.com'),
      array('index' => '7', 'name' => 'Billy Hughes', 'address' => '21 Prime st.', 'phone' => '1357 2468', 'email' => 'b.hughes@gmail.com'),
      array('index' => '8', 'name' => 'Stanley Bruce', 'address' => '22 Prime st.', 'phone' => '2468 3579', 'email' => 's.bruce@yahoo.com'),
      array('index' => '9', 'name' => 'James Scullin', 'address' => '23 Prime st.', 'phone' => '1212 2323', 'email' => 'j.scullin@gmail.com'),
      array('index' => '10', 'name' => 'Joseph Lyons', 'address' => '24 Prime st.', 'phone' => '3434 4545', 'email' => 'j.lyons@unaus.com'),
      array('index' => '11', 'name' => 'Earle Page', 'address' => '25 Prime st.', 'phone' => '5656 6767', 'email' => 'e.page@country.com'),
      array('index' => '12', 'name' => 'Robert Menzies', 'address' => '26 Prime st.', 'phone' => '7878 8989', 'email' => 'r.menzies@unaus.com'),
      array('index' => '13', 'name' => 'Arthur Fadden', 'address' => '27 Prime st.', 'phone' => '9090 0101', 'email' => 'a.fadden@country.com'),
      array('index' => '14', 'name' => 'John Curtin', 'address' => '28 Prime st.', 'phone' => '0987 6543', 'email' => 'j.curtin@gmail.com'),
      array('index' => '15', 'name' => 'Frank Forde', 'address' => '29 Prime st.', 'phone' => '9876 5432', 'email' => 'f.forde@gmail.com'),
      array('index' => '16', 'name' => 'Ben Chifley', 'address' => '30 Prime st.', 'phone' => '8765 4321', 'email' => 'b.chifley@gmail.com'),
      array('index' => '-', 'name' => 'Robert Menzies', 'address' => '31 Prime st.', 'phone' => '7654 3210', 'email' => 'r.menzies@hotmail.com'),
      array('index' => '17', 'name' => 'Harold Holt', 'address' => '32 Prime st.', 'phone' => '6543 2109', 'email' => 'h.holt@hotmail.com'),
      array('index' => '18', 'name' => 'John McEwen', 'address' => '33 Prime st.', 'phone' => '5432 1098', 'email' => 'j.mcewen@country.com'),
      array('index' => '19', 'name' => 'John Gorphonen', 'address' => '34 Prime st.', 'phone' => '4321 0987', 'email' => 'j.gorphonen@hotmail.com'),
      array('index' => '21', 'name' => 'Gough Whitlam', 'address' => '35 Prime st.', 'phone' => '3210 9876', 'email' => 'g.whitlam@gmail.com'),
      array('index' => '22', 'name' => 'Malcolm Fraser', 'address' => '36 Prime st.', 'phone' => '2109 8765', 'email' => 'm.fraser@hotmail.com'),
      array('index' => '23', 'name' => 'Bob Hawke', 'address' => '37 Prime st.', 'phone' => '1098 7654', 'email' => 'b.hawke@gmail.com'),
      array('index' => '24', 'name' => 'Paul Keating', 'address' => '38 Prime st.', 'phone' => '6969 9696', 'email' => 'p.keating@gmail.com'),
      array('index' => '25', 'name' => 'John Howard', 'address' => '39 Prime st.', 'phone' => '4204 2042', 'email' => 'j.howard@hotmail.com'),
      array('index' => '26', 'name' => 'Kevin Rudd', 'address' => '40 Prime st.', 'phone' => '4242 2424', 'email' => 'k.rudd@gmail.com'),
      array('index' => '27', 'name' => 'Julia Gillard', 'address' => '41 Prime st.', 'phone' => '8008 1355', 'email' => 'j.gillard@gmail.com')
  );
  return $pms;
}
?>

