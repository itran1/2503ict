
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    
    <title>Social Network</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
  </head>

  <body>

    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Social Network</a>
        </div>
        
        <div id="navbar" class="navbar-collapse collapse">
        <!--  <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
          -->
          <ul class="nav navbar-nav navbar-right">
            <li><a href="../navbar/">Photos</a></li>
            <li><a href="./">Friends<span class="sr-only">(current)</span></a></li>
            <li><a href="../navbar-fixed-top/">Login</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>


    <div class="container">

      <!-- Main component for a primary marketing message or call to action -->
      <div class='row'>
        
        <div class='col-md-4'>
          <form>
            <div id="nameInput">
                Name: <br>
                <input type="text" name="name"/> <br>
            </div>
            <div id="messageInput">
                Message: <br>
                <textarea rows="4" cols="25">Enter your message/post</textarea> <br>
                
            </div>
            <button>Post</button>
          </form>
        </div>
        
        <div class="col-md-8">
          <div class="post">
            <img class="photo" src="images/Bird.jpg" alt="Bird">
            Date: 21/12/12 <br>
            Message: Hello, World 1
          </div>
            <div class="post">
            <img class="photo" src="images/Bird.jpg" alt="Bird">
            Date: 21/12/12 <br>
            Message: Hello, World 2
          </div>
            <div class="post">
            <img class="photo" src="images/Bird.jpg" alt="Bird">
            Date: 21/12/12 <br>
            Message: Hello, World 3
          </div>
        </div>
      </div>

    </div> <!-- /container -->


    
  </body>
</html>
